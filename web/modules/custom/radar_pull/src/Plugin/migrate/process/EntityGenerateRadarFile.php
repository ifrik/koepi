<?php

namespace Drupal\radar_pull\Plugin\migrate\process;

use Drupal\migrate_plus\Plugin\migrate\process\EntityGenerate;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin generates, and downloads, within the process plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_generate_radar_file"
 * )
 */
class EntityGenerateRadarFile extends EntityGenerate {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $this->migrateExecutable = $migrateExecutable;
    $this->fileData = $row->getSourceProperty($this->configuration['source_property']);
    if (isset($value[$this->configuration['property']])) {
      $result = parent::transform($value, $migrateExecutable, $row, $destinationProperty);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function entity($value) {
    $entity_values = [$this->lookupValueKey => $value];

    if ($this->lookupBundleKey) {
      $entity_values[$this->lookupBundleKey] = $this->lookupBundle;
    }

    // Gather any static default values for properties/fields.
    if (isset($this->configuration['default_values']) && is_array($this->configuration['default_values'])) {
      foreach ($this->configuration['default_values'] as $key => $value) {
        $entity_values[$key] = $value;
      }
    }

    $entity_values['name'] = $this->fileData['filename'];
    $new_row = new Row([
      'url' => $this->fileData['url'],
      'destination' => 'public://' . $this->fileData['filename'],
    ], []);
    $this->migrateExecutable->processRow($new_row, [
      'uri' => [
        'plugin' => 'download',
        'source' => [
          'url',
          'destination',
        ],
      ],
    ]);
    $destination = $new_row->getDestination();

    if (!empty($destination['uri'])) {
      $file = $this->entityManager
        ->getStorage('file')
        ->create([
          'uri' => $destination['uri'],
          'filename' => $this->fileData['filename'],
        ]);

      $file->setPermanent();
      $file->save();
    }

    $entity_values[$this->configuration['destination_subproperty']] = $file->id();

    return $entity_values;
  }

}
