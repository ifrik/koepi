<?php

namespace Drupal\radar_pull\Plugin\migrate\process;

use Drupal\migrate_plus\Plugin\migrate\process\EntityGenerate;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin generates entities within the process plugin.
 *
 * This can probably be done by chaining some plugins, but I've not managed yet,
 * and this is simple. It allows multiple, and will then grab the correct
 * property.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_generate_multiple_property"
 * )
 */
class EntityGenerateMultipleProperty extends EntityGenerate {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    if (isset($value[$this->configuration['property']])) {
      $result = parent::transform($value[$this->configuration['property']], $migrateExecutable, $row, $destinationProperty);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}
